/**
 * PCFCMS 程序中常用到的JS，封装在此
 * @网站 http://www.pcfcms.com
 * @日期 2021年01月01日
**/
;(function($){
	$.checkbox = {
		_obj:function(id)
		{
			if(id && id != 'undefined' && typeof id == 'string'){
				if(id.match(/^[a-zA-Z0-9\-\_]{1,}$/)){
					if($("#"+id).is('input')){
						return $("#"+id);
					}
					return $("#"+id+" input[type=checkbox]");
				}
				if($(id).is('input')){
					return $(id);
				}
				return $(id+" input[type=checkbox]");
			}
			return $("input[type=checkbox]");
		},
		/*全选*/
		all:function(id)
		{
			var obj = this._obj(id);
			obj.prop('checked',true);
            window.setTimeout("layui.form.render('checkbox')",100);
			return true;
		},
		/*返先*/
		none:function(id)
		{
			var obj = this._obj(id);
			obj.removeAttr('checked');
            window.setTimeout("layui.form.render('checkbox')",100);
			return true;
		},
		/*反选*/
		anti:function(id)
		{
			var t = this._obj(id);
			t.each(function(i){
				if($(this).is(":checked")){
					$(this).removeAttr('checked');
				}else{
					$(this).prop('checked',true);
				}
				window.setTimeout("layui.form.render('checkbox')",100)
			});
		}
	}
})(jQuery);

/*操作类*/
;(function($){
	$.input = {
		//全选
		checkbox_all: function(id)
		{
			return $.checkbox.all(id);
		},
		//全不选
		checkbox_none: function(id)
		{
			return $.checkbox.none(id);
		},
		//反选
		checkbox_anti: function(id)
		{
			return $.checkbox.anti(id);
		}
	};
})(jQuery);
