<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
// [ 应用入口文件 ]
namespace think;
//检测PHP环境
if(version_compare(PHP_VERSION,'7.1.0','<'))  die('本系统要求PHP版本 >= 7.1.0，当前PHP版本为：'.PHP_VERSION . '，请到虚拟主机控制面板里切换PHP版本，或联系空间商协助切换。<a href="http://www.pcfcms.com" target="_blank">点击查看安装教程</a>');
error_reporting(E_ERROR | E_WARNING | E_PARSE);//报告运行时错误

// PUBLIC目录运行
define('WWW_ROOT',dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('PUBLIC_PATH', WWW_ROOT);
define('PUBLIC_ROOT', "");

// 插件目录
define('ADDONS_PATH', WWW_ROOT .'addons'.DIRECTORY_SEPARATOR); 

//前端缓存时间
define('HOME_CACHE_TIME', 86400);

//后端缓存时间
define('ADMIN_CACHE_TIME', 86400);

// 检测是否已安装PCFCMS系统
if(file_exists(WWW_ROOT.'public/install/') && !file_exists(WWW_ROOT.'public/install/install.lock')){
    header('Location:/install/');exit;
}

// PUBLIC目录运行
require __DIR__ . '/../vendor/autoload.php';

// 执行HTTP应用并响应
$http = (new App())->http;
$response = $http->name('home')->run();
$response->send();
$http->end($response);