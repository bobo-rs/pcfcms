<?php
/***********************************************************
 * 错误控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\api\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;
class Error
{
    public function __call($method, $args)
    {
        return "Api应用访问的控制器不存在！";
    }

}
