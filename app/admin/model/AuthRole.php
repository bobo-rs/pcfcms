<?php
/***********************************************************
 * 角色权限
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Request;
class AuthRole extends Common
{
    //权限组列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('auth_role')
                ->field($tableWhere['field'])
                ->where($tableWhere['where'])
                ->order($tableWhere['order'])
                ->paginate($limit)->toArray(); 
        $pcfdata = $list['data'];
        $newdata = [];
        foreach ($pcfdata as $key => $value) {
            $newdata[$key]['id'] = $value['id'];
            $newdata[$key]['name'] = $value['name'];
            $newdata[$key]['built_in'] = $value['built_in'];
            $newdata[$key]['remark'] = $value['remark'];
            $newdata[$key]['online_update'] = $value['online_update'];
            $newdata[$key]['status'] = $value['status'];
            $newdata[$key]['only_oneself'] = $value['only_oneself'];
            $newdata[$key]['add_time'] = pcftime($value['add_time']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list['total'],'data' => $newdata];
        return $result;
    }

    //查询条件
    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['name']) && $post['name'] != "") {
            $where[] = ['name', 'like', '%' . $post['name'] . '%'];
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "id desc";
        return $result;
    }

    //添加|编辑
    public function toAdd($data)
    {
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            if(!isset($data['name']) || empty($data['name'])){
                $result = ['code' => 1, 'msg' => '请输入角色名'];
                return $result;  
            }
            $where = [];
            $where[] = ['name','=',$data['name']];
            $where[] = ['id', '<>', $data['id']];
            $count = Db::name('auth_role')->where($where)->count();
            if($count > 0){
                $result = ['code' => 1, 'msg' => '该权限组名称已存在，请检查'];
                return $result;  
            }
            $data = array(
                'id' => isset($data['id']) ? $data['id'] : '',
                'name' => isset($data['name']) ? $data['name'] : '',
                'status' => isset($data['status']) ? $data['status'] : 0,
                'online_update' => isset($data['online_update']) ? 1 : 0,
                'only_oneself' => isset($data['only_oneself']) ? 1 : 0,
                'permission' => isset($data['popedom']) ? serialize($data['popedom']) : '',
                'remark' => isset($data['remark']) ? $data['remark'] : '',
                'update_time' => getTime(),
            );
            if(Db::name('auth_role')->save($data)){
                $result = ['code' => 0, 'msg' => '操作成功','url' => url('/authrole/index')->suffix(false)->domain(true)->build()];
                return $result;                
            }else{
                $result = ['code' => 0, 'msg' => '操作成功'];
                return $result; 
            }
        } else {
            if(!isset($data['name']) || empty($data['name'])){
                $result = ['code' => 1, 'msg' => '请输入角色名'];
                return $result;  
            }
            $count = Db::name('auth_role')->where('name', $data['name'])->count();
            if($count > 0){
                $result = ['code' => 1, 'msg' => '该权限组名称已存在，请检查'];
                return $result;
            }
            $data = array(
                'name' => isset($data['name']) ? $data['name'] : '',
                'status' => isset($data['status']) ? $data['status'] : 0,
                'online_update' => isset($data['online_update']) ? 1 : 0,
                'only_oneself' => isset($data['only_oneself']) ? 1 : 0,
                'permission' => isset($data['popedom']) ? serialize($data['popedom']) : '',
                'admin_id' => Session::get('admin_id'),
                'remark' =>isset($data['remark']) ? $data['remark'] : '',
                'add_time' => getTime(),
            );
            if(Db::name('auth_role')->save($data)){
                $result = ['code' => 0, 'msg' => '操作成功','url' => url('/authrole/index')->suffix(false)->domain(true)->build()];
                return $result; 
            }else{
                $result = ['code' => 1, 'msg' => '操作失败'];
                return $result; 
            }
        }
    }

}
