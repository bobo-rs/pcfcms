<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use app\admin\model\AdminLog as AdminLogModel;
class Adminlog extends Base
{
    public $AdminLogModel;
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $this->AdminLogModel = new AdminLogModel();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index(){
        //验证权限
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            return $this->AdminLogModel->tableData(input('param.'));
        }
        return $this->fetch();  
    }

    //清除三个月之前日志
    public function clear_log_chache()
    {
        //验证权限
        if(!$this->popedom["delete"]){
            if(config('params.auth_msg.test')){
                $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                return $result;                    
            }
        }
        $before_time = date('Y-m-d 00:00:00', strtotime('-3 month'));
        $where[]=['log_time','<',$before_time];
        if (false !== Db::name('adminLog')->where($where)->delete()) {
            $result = ['status' => true, 'msg' => '删除成功'];
        } else {
            $result = ['status' => false, 'msg' => '删除失败'];
        }
        return $result;
    }

}
