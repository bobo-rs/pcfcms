<?php
/***********************************************************
 * 站点地图
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use app\admin\controller\Base;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
class Sitemap extends Base
{

    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }

    // 配置入口
    public function index()
    {
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        } 
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $param = input('param.');
            $param['sitemap_auto'] = isset($param['sitemap_auto']) ? $param['sitemap_auto'] : 0;
            $param['sitemap_xml'] = isset($param['sitemap_xml']) ? $param['sitemap_xml'] : 0;
            $param['sitemap_archives_num'] = isset($param['sitemap_archives_num']) ? intval($param['sitemap_archives_num']) : 100;
            sysConfig('sitemap',$param);
            Cache::clear();
            $admin_temp = glob(WWW_ROOT.'runtime/admin/temp/'.'*.php');
            array_map('unlink', $admin_temp);
            $result = ['status'=> true, 'msg'=> '设置成功'];
            return $result;   
        }
        $config = sysConfig("sitemap");
        $this->assign('config',$config);
        return $this->fetch();
    }

    // 生成地图
    public function sitemap()
    {
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            sitemap_all();
            $result = ['status'=> true, 'msg'=> '生成成功'];
            return $result;   
        } 
    }


}
