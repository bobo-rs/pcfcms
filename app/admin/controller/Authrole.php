<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use app\admin\model\AuthRole as AuthRoleModel;
class Authrole extends Base
{
    public $AuthRoleModel;
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $this->AuthRoleModel = new AuthRoleModel();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }

    //列表
    public function index(){
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            return $this->AuthRoleModel->tableData(input('param.'));
        }
        return $this->fetch();
    }

    //添加
    public function add()
    {
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            return $this->AuthRoleModel->toAdd(input('param.'));
        }
        //获取全部权限菜单
        $getauthmenulist = Db::name('menu')->where(['parent_id'=>0,'is_show'=>1])->order('sort asc')->column('*', 'id');
        foreach ($getauthmenulist as $key => $value) {
            $getauthmenulist[$key]['child'] = getchandList1($value['id']);
        }
        $role_id =  Session::get('admin_info.role_id');
        if($role_id > 0){
            $admin_popedom = Session::get('admin_info.auth_role_info.permission');
            $this->assign('admin_popedom',$admin_popedom);
        }else{
            $admin_popedom = -1;
            $this->assign('admin_popedom',$admin_popedom);
        }
        $this->assign('authmenu',$getauthmenulist);
        return $this->fetch();
    }

    //编辑
    public function edit()
    {
        $id = input('param.id');
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            } 
            return $this->AuthRoleModel->toAdd(input('param.'));
        }
        //获取全部权限菜单
        $getauthmenulist = Db::name('menu')->where(['parent_id'=>0,'is_show'=>1])->order('sort asc')->column('*', 'id');
        foreach ($getauthmenulist as $key => $value) {
            $getauthmenulist[$key]['child'] = getchandList1($value['id']);
        }
        $authinfo = Db::name('auth_role')->where('id',$id)->find();
        $this->assign("plist",unserialize($authinfo['permission']));
        $this->assign('authinfo',$authinfo);
        $role_id =  Session::get('admin_info.role_id');
        if($role_id > 0){
            $admin_popedom = 1;
        }else{
            $admin_popedom = -1;
        }
        $this->assign('admin_popedom',$admin_popedom);
        $this->assign('authmenu',$getauthmenulist);
        return $this->fetch();
    }

    //删除
    public function del()
    {
        if (Request::isPost()) {
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id = input('param.del_id/d');
            //删除前判断是否有管理员关联
            if(Db::name('admin')->where("role_id",$id)->find()){
                $result = ['status' => false, 'msg' => '已有管理员使用'];
                return $result; 
            }
            if(empty($id)){
                $result = ['status' => false, 'msg' => '参数丢失'];
                return $result;  
            }
            if (Db::name('auth_role')->where("id",$id)->delete()) {
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result;
            }
        }       
    }

}
