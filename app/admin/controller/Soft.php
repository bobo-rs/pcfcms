<?php
/***********************************************************
 * 下载模型
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Cache;
use app\admin\model\Content as Contentmodel;
use app\admin\model\Archives as Archivesmodel;
use app\admin\logic\FieldLogic;
class Soft extends Base
{
    
    public $nid = "soft";// 模型标识
    public $channeltype;// 模型ID
    public $Contentmodel;
    public $FieldLogic;
    public $Archivesmodel;
    public $popedom;
    public function _initialize() 
    {
        parent::_initialize();
        $channeltype_list = config('pcfcms.channeltype_list');
        $this->Contentmodel = new Contentmodel();
        $this->Archivesmodel = new Archivesmodel();
        $this->FieldLogic = new FieldLogic();
        $this->channeltype = $channeltype_list[$this->nid];
        $this->assign('channel', $this->channeltype);
        $this->assign('admin_info', session::get('admin_info'));
        $ctl_act = strtolower('content/index');
        $this->popedom = appfile_popedom($ctl_act); 
    }

    // 自定义文档列表
    public function index()
    {
        //验证权限
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        } 
        if (Request::isAjax()) {
            return $this->Contentmodel->tableData(input('param.'));
        }
        $typeid = !empty(input('param.typeid/d')) ? input('param.typeid/d') : 0;
        $assign_data['typeid'] = $typeid;
        // 栏目
        $arctype_html = allow_release_arctype($typeid,[$this->channeltype],true,true);
        $assign_data['arctype_html'] = $arctype_html;
        $this->assign($assign_data);
        return $this->fetch('content/soft_index');
    }

    // 添加
    public function add()
    {
        if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            $post = input('param.');
            $typeid = input('param.typeid/d', 0);
            if (empty($typeid)) {
                $result = ['status' => false,'msg' => '请选择所属栏目！'];
                return $result;
            }
            // 根据标题自动提取相关的关键字
            $seo_keywords = $post['seo_keywords'];
            if (!empty($seo_keywords)) {
                $seo_keywords = str_replace('，', ',', $seo_keywords);
            }
            // 是否有封面图
            if (empty($post['litpic'])) {
                $is_litpic = 0; // 无封面图
            } else {
                $is_litpic = 1; // 有封面图
            }
            // 外部链接跳转
            $jumplinks = '';
            $is_jump = isset($post['is_jump']) ? $post['is_jump'] : 0;
            if (intval($is_jump) > 0) {
                $jumplinks = $post['jumplinks'];
            }
            if ($post['type_tempview'] == $post['tempview']) {
                unset($post['type_tempview'],$post['tempview']);
            }else{
                unset($post['type_tempview']);
            }
            // 存储数据
            $newData = [
                'typeid'=> $typeid,
                'channel'   => $this->channeltype,
                'is_b'      => empty($post['is_b']) ? 0 : $post['is_b'],
                'is_head'     => empty($post['is_head']) ? 0 : $post['is_head'],
                'is_special'  => empty($post['is_special']) ? 0 : $post['is_special'],
                'is_recom'    => empty($post['is_recom']) ? 0 : $post['is_recom'],
                'is_jump'     => $is_jump,
                'is_litpic'   => $is_litpic,
                'jumplinks' => $jumplinks,
                'seo_keywords'  => $seo_keywords,
                'seo_description'  => $post['seo_description'],
                'admin_id'  => session::get('admin_id'),
                'sort_order'   => 100,
                'add_time'     => strtotime($post['add_time']),
                'update_time'  => strtotime($post['add_time']),
                'show_time'    => getTime()
            ];
            $data = array_merge($post, $newData);
            $insertdata = array_merge($post, $newData);
            unset($insertdata['addonFieldExt']);
            $aid = Db::name('archives')->insertGetId($insertdata);
            if ($aid){
                $this->Archivesmodel->afterSave($aid,$data);
                $result = ['status'=>true,'msg'=>'添加成功','url'=>url('/soft/index',['current_channel'=>$this->channeltype,'typeid'=>$typeid])->suffix(true)->domain(true)->build()];
                return $result;
            }
            $result = ['status' => false,'msg' => '添加失败'];
            return $result;
        }
        $assign_data = [];
        $typeid = !empty(input('param.typeid/d')) ? input('param.typeid/d') : 0;
        $assign_data['typeid'] = $typeid;
        // 允许发布文档列表的栏目
        $arctype_html = allow_release_arctype($typeid,[$this->channeltype],true,true);
        $assign_data['arctype_html'] = $arctype_html;
        // 自定义字段
        $addonFieldExtList = $this->FieldLogic->getChannelFieldList($this->channeltype);
        $assign_data['addonFieldExtList'] = $addonFieldExtList;
        // 阅读权限
        $arcrank_list = get_arcrank_list();
        $assign_data['arcrank_list'] = $arcrank_list;
        // 模板列表
        $templateList = $this->Archivesmodel->getTemplateList($this->nid);
        $this->assign('templateList', $templateList);
        // 默认模板文件
        $tempview = 'view_'.$this->nid.'.'.config('view.view_suffix');
        $this->assign('tempview', $tempview);
        $this->assign($assign_data);
        return $this->fetch('content/soft_add');
    }

    // 编辑
    public function edit()
    {
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }  
            $post = input('param.');
            $typeid = input('param.typeid/d', 0);
            if (empty($typeid)) {
                $result = ['status' => false,'msg' => '请选择所属栏目！'];
                return $result;
            }
            // 根据标题自动提取相关的关键字
            $seo_keywords = $post['seo_keywords'];
            if (!empty($seo_keywords)) {
                $seo_keywords = str_replace('，', ',', $seo_keywords);
            } 
            // 自动获取内容第一张图片作为封面图
            if (empty($post['litpic']) && !empty(input('param.addonFieldExt.content'))) {
                $post['litpic'] = get_html_first_imgurl(input('param.addonFieldExt.content'));
            }
            // 是否有封面图
            if (empty($post['litpic'])) {
                $is_litpic = 0; // 无封面图
            } else {
                $is_litpic = 1; // 有封面图
            }
            // SEO描述
            $seo_description = '';
            if (empty($post['seo_description']) && !empty(input('param.addonFieldExt.content'))) {
                $seo_description = pcf_msubstr(checkStrHtml(input('param.addonFieldExt.content')),0,250,false);
            } else {
                $seo_description = $post['seo_description'];
            }
            // 外部链接
            $jumplinks = '';
            $is_jump = isset($post['is_jump']) ? $post['is_jump'] : 0;
            if (intval($is_jump) > 0) {
                $jumplinks = $post['jumplinks'];
            }
            if ($post['type_tempview'] == $post['tempview']) {
                unset($post['type_tempview'],$post['tempview']);
            }else{
                unset($post['type_tempview']);
            }
            // 存储数据
            $newData = [
                'typeid'=> $typeid,
                'channel'   => $this->channeltype,
                'is_b'      => empty($post['is_b']) ? 0 : $post['is_b'],
                'is_head'   => empty($post['is_head']) ? 0 : $post['is_head'],
                'is_special'=> empty($post['is_special']) ? 0 : $post['is_special'],
                'is_recom'  => empty($post['is_recom']) ? 0 : $post['is_recom'],
                'is_jump'   => $is_jump,
                'is_litpic' => $is_litpic,
                'jumplinks' => $jumplinks,
                'seo_keywords'     => $seo_keywords,
                'seo_description'  => $seo_description,
                'add_time'     => strtotime($post['add_time']),
                'update_time'  => getTime()
            ];
            $data = array_merge($post, $newData);
            $editdata = array_merge($post, $newData);
            unset($editdata['addonFieldExt']);
            $r = Db::name('archives')->save($editdata);
            if ($r) {
                $this->Archivesmodel->afterSave($data['aid'],$data);
                $result = ['status'=>true,'msg'=>'编辑成功','url'=>url('/soft/index',['current_channel'=>$this->channeltype,'typeid'=>$typeid])->suffix(true)->domain(true)->build()];
                return $result;
            }
            $result = ['status' => false,'msg' => '操作失败'];
            return $result;
        }
        $assign_data = [];
        $id = input('param.id/d',0);
        $info = $this->Archivesmodel->getInfo($id,null,true);
        if (empty($info)) {
            return $this->Notice("数据不存在，请联系管理员！",url('/soft/index',['current_channel'=>$this->channeltype])->suffix(true)->domain(true)->build(),3,false);
        }
        $admin_info = session::get('admin_info');
        if (0 < intval($admin_info['role_id'])) {
            $auth_role_info = $admin_info['auth_role_info'];
            if(!empty($auth_role_info)){
                if(isset($auth_role_info['only_oneself']) && (1 == $auth_role_info['only_oneself']) && ($admin_info['admin_id'] != $info['admin_id'])){
                    return $this->Notice("您不能操作他人文档",url('/soft/index',['current_channel'=>$this->channeltype])->suffix(true)->domain(true)->build(),3,false);
                }
            }
        }
        // SEO描述
        if (!empty($info['seo_description'])) {
            $info['seo_description'] = pcf_msubstr(checkStrHtml($info['seo_description']),0,config('pcfcms.arc_seo_description_length'),false);
        }
        $assign_data['field'] = $info;
        // 允许发布文档列表的栏目
        $arctype_html = allow_release_arctype($info['typeid'],[$this->channeltype],true,true);
        $assign_data['arctype_html'] = $arctype_html;
        // 自定义字段
        $addonFieldExtList = $this->FieldLogic->getChannelFieldList($this->channeltype,0,$id,$info);
        $assign_data['addonFieldExtList'] = $addonFieldExtList;
        // 阅读权限
        $arcrank_list = get_arcrank_list();
        $assign_data['arcrank_list'] = $arcrank_list;
        // 模板列表
        $templateList = $this->Archivesmodel->getTemplateList($this->nid);
        $this->assign('templateList', $templateList);
        // 默认模板文件
        $tempview = 'view_'.$this->nid.'.'.config('view.view_suffix');
        $this->assign('tempview', $tempview);
        $this->assign($assign_data);
        return $this->fetch('content/soft_edit');
    }

}