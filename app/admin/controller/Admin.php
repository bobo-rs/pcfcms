<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use app\admin\model\Admin as AdminModel;
class Admin extends Base
{
    public $AdminModel;
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $this->AdminModel = new AdminModel();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }
    
    //列表
    public function index(){
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            return $this->AdminModel->tableData(input('param.'));
        }
        //获取当前管理员
        $admin_id = Db::name('admin')->where('admin_id',session::get('admin_info.admin_id'))->value('admin_id');
        $this->assign('admin_id',$admin_id);
        return $this->fetch();
    }

    //添加
    public function add(){
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            return $this->AdminModel->toAdd(input('param.'));
        }
        $RoleList = Db::name('auth_role')->where('status', 1)->select()->toArray();
        $this->assign('roleList', $RoleList);
        //获取当前管理员
        $role_id = Db::name('admin')->where('admin_id',session::get('admin_info.admin_id'))->value('role_id');
        $this->assign('role_id',$role_id);
        return $this->fetch('add');
    }

    //编辑
    public function edit(){
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                } 
            } 
            return $this->AdminModel->toAdd(input('param.'));
        }
        $manageInfo = Db::name('admin')->where(['admin_id' => input('get.id/d')])->find();//管理员
        $manageRoleList = Db::name('auth_role')->where('status', 1)->select()->toArray();//管理组
        foreach ($manageRoleList as $k => $v) {
            if ($manageInfo['role_id'] == $v['id']) {
               $manageRoleList[$k]['checked'] = true;
            }else{
               $manageRoleList[$k]['checked'] = false;
            }
        }
        $admin_info = Session::get('admin_info');
        $this->assign('sess_admin_info', $admin_info);
        $this->assign('roleList', $manageRoleList);
        $this->assign('manageInfo', $manageInfo);
        return $this->fetch('edit');
    }
    
    //删除
    public function del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $admin_id = input('param.del_id/d');
            //超级管理员不能删除
            if($admin_id == 1){
                $result = ['status' => false, 'msg' => '超级管理员不允许删除'];
                return $result;
            }
            //不能删除自己
            if($admin_id == Session::get('admin_id')){
                $result = ['status' => false, 'msg' => '不允许删除自己'];
                return $result;
            }
            if (Db::name('admin')->where("admin_id",$admin_id)->delete()) {
                Db::name('admin_log')->where("admin_id",$admin_id)->delete();
                $result = ['status' => true, 'msg' => '删除成功'];
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
            }
            return $result;
        }       
    }

    //重置密码
    public function restPsw(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $admin_id = input('param.admin_id/d');
            $manageInfo = Db::name('admin')->where('admin_id', $admin_id)->find();
            $newpassword = func_encrypt('123456');
            if($manageInfo['password'] == $newpassword){
                $result = ['status' => true, 'msg' => '密码已经是【123456】'];
                return $result;
            }
            if (Db::name('admin')->where('admin_id', $admin_id)->data(['password' => $newpassword])->update()) {
                $result = ['status' => true, 'msg' => '密码已经是【123456】'];
            } else {
                $result = ['status' => false, 'msg' => '重置失败'];
            }
            return $result;
        }         
    }

    //修改用户信息
    public function userinfo(){
        if (Request::isPost()) {
            if(!$this->popedom["userinfo"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.other')];
                    return $result;                    
                }
            }
            return $this->AdminModel->toAdd(input('param.'));
        }
        $admin_id = Session::get('admin_id');
        $admininfo = Db::name('admin')->where('admin_id' , $admin_id)->find();
        if (!$admininfo) {
            return $this->Notice("获取管理员信息失败！",true,3,false);
        }
        $this->assign('admininfo', $admininfo);
        return $this->fetch();
    }

}
