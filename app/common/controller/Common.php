<?php
/***********************************************************
 * 控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\common\controller;
use think\facade\Db;
use think\facade\Session;
use app\BaseController;
class Common extends BaseController 
{

	public $view_suffix = "html";
    // 初始化操作
    public function _initialize() {
        parent::_initialize();
        header("Cache-control: private");// history.back返回后输入框值丢失问题 
        //关闭网站
        if (empty(sysConfig('web.web_status'))) {
            die("<!DOCTYPE HTML><html><head><title>网站暂时关闭</title><body><div style='text-align:center;font-size:20px;font-weight:bold;margin:50px 0px;'>网站暂时关闭，维护中……</div></body></html>");
        }
    }

}