<?php
/***********************************************************
 * 基础控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\home\controller;
use app\common\controller\Common;
class Base extends Common
{

    public function _initialize()
    {
        parent::_initialize();
    }

}
