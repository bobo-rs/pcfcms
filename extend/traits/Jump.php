<?php
namespace traits;
use think\Response;
use think\facade\Config;
use think\facade\Request;
use think\facade\View as ViewTemplate;
use think\exception\HttpResponseException;
trait Jump
{
    /**
     * 操作成功跳转的快捷方法.
     * @param mixed  $msg    提示信息
     * @param string $url    跳转的 URL 地址
     * @param mixed  $data   返回的数据
     * @param int    $wait   跳转等待时间
     * @param array  $header 发送的 Header 信息
     */
    protected function success($msg = '', $url = '', $data = '', $wait = 3, array $header = [])
    {
        if (is_null($url) && !is_null(Request::server('HTTP_REFERER'))) {
            $url = Request::server('HTTP_REFERER');
        } elseif (true !== $url && '' !== $url && is_string($url) && !strpos($url, '://') && 0 !== strpos($url, '/')) {
            $url = U($url);
        } elseif (is_array($msg)) {
            $data = $msg;
            $msg = '';
        } elseif (is_array($url)) {
            $data = $url;
            $url = '';
        }
        $type = $this->getResponseType();
        $result = [
            'code' => 1,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];
        if ('html' == strtolower($type)) {
            $result = ViewTemplate::fetch(Config::get('app.dispatch_success_tmpl'), $result);
            if (method_exists($this, 'replace_string')) {
                $result = $this->replace_string($result);
            }
        }
        $response = Response::create($result, $type)->header($header);
        throw new HttpResponseException($response);
    }

    /**
     * 操作错误跳转的快捷方法.
     * @param mixed  $msg    提示信息
     * @param string $url    跳转的 URL 地址
     * @param mixed  $data   返回的数据
     * @param int    $wait   跳转等待时间
     * @param array  $header 发送的 Header 信息
     */
    protected function error($msg = '', $url = '', $data = '', $wait = 3, array $header = [])
    {
        if (is_null($url)) {
            $url = Request::isAjax() ? '' : 'javascript:history.back(-1);';
        } elseif (true !== $url && '' !== $url && is_string($url) && !strpos($url, '://') && 0 !== strpos($url, '/')) {
            $url = U($url);
        } elseif (is_array($msg)) {
            $data = $msg;
            $msg = '';
        } elseif (is_array($url)) {
            $data = $url;
            $url = '';
        }
        $type = $this->getResponseType();
        $result = [
            'code' => 0,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];
        if ('html' == strtolower($type)) {
            $result = ViewTemplate::fetch(Config::get('app.dispatch_success_tmpl'), $result);
            if (method_exists($this, 'replace_string')) {
                $result = $this->replace_string($result);
            }
        }
        $response = Response::create($result, $type)->header($header);
        throw new HttpResponseException($response);
    }

    // 获取当前的 response 输出类型
    protected function getResponseType()
    {
        return Request::isAjax() ? Config::get('app.default_ajax_return') : Config::get('app.default_return_type');
    }
    
}
