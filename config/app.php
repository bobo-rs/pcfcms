<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host'         => '',
    
    // 应用的命名空间
    'app_namespace'    => '',
    
    // 压缩html
    'html_compress' => true,

    // 是否启用路由
    'with_route'       => true,

    // 是否启用事件
    'with_event'       => true,

    // 自动多应用模式
    'auto_multi_app'   => false,

    // 默认应用
    'default_app'    => 'home',

    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => ['*'=>'home', 'api'=>'api', 'admin'=>'admin','home'=>'home'],

    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => ['common','extra'],

    // 默认输出类型
    'default_return_type'   => 'html',
    // 默认AJAX 数据返回格式,可选json xml
    'default_ajax_return'   => 'json',

    // 异常页面的模板文件
    'exception_tmpl' => app()->getThinkPath().'tpl/think_exception.tpl',
    // 默认跳转页面对应的模板文件
    'dispatch_error_tmpl' => app()->getRootPath().'/public/jump.html',
    // 默认成功跳转对应的模板文件
    'dispatch_success_tmpl' => app()->getRootPath().'/public/jump.html',

    // 错误显示信息,非调试模式有效
    'error_message'    => '请联系技术处理：QQ 1131680521',
    // 显示错误信息
    'show_error_msg'   => true


];